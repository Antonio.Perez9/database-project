package com.proyectsjala.springbootmongodb.repository;

import com.proyectsjala.springbootmongodb.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.proyectsjala.springbootmongodb.model.Task.Status;
import org.springframework.data.mongodb.repository.Query;
import java.time.LocalDateTime;
import java.util.List;

import java.util.List;

public interface TaskRepository extends MongoRepository<Task, String> {


    @Query("{'status' : {$in : ?0}, 'idUser' : ?1}")
    List<Task> findByStatus(List<String> status, String userId);
    @Query("{'idUser' : ?0, 'status' : {$ne : 'DELETED'}}")
    List<Task> findByidUser(String idUser, Status deleted);


    @Query("{'idUser' : ?0, 'status' : {$ne : 'DELETED'}, 'dateFinally' : {'$gte' : ?1, '$lte' : ?2}}")

    List<Task> findByidUserAndDateCreationBetween(String idUser, LocalDateTime startDate, LocalDateTime endDate);

    @Query("{'status' : ?0, 'deletionDate' : {$lt : ?1}}")
    List<Task> findByStatusAndDeletionDateBefore(Task.Status status, LocalDateTime deletionDate);



}
