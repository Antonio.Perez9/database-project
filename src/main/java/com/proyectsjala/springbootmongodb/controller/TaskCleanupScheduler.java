package com.proyectsjala.springbootmongodb.controller;

import com.proyectsjala.springbootmongodb.model.Task;
import com.proyectsjala.springbootmongodb.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class TaskCleanupScheduler {

    private static final long DAYS_TO_KEEP_DELETED_TASKS = 15;
    private static final long MINUTES_TO_KEEP_DELETED_TASKS = 10;
    @Autowired
    private TaskRepository taskRepository;

    @Scheduled(fixedRate = 600000) // Se ejecuta cada 10 minutos (10 * 60 * 1000 milisegundos)
    public void cleanUpDeletedTasks() {
        LocalDateTime deletionThreshold = LocalDateTime.now().minusMinutes(DAYS_TO_KEEP_DELETED_TASKS);
        List<Task> tasksToDelete = taskRepository.findByStatusAndDeletionDateBefore(Task.Status.DELETED, deletionThreshold);

        for (Task task : tasksToDelete) {
            taskRepository.delete(task);
        }
    }
}