package com.proyectsjala.springbootmongodb.controller;

import com.proyectsjala.springbootmongodb.model.User;
import com.proyectsjala.springbootmongodb.model.UserCredentials;
import com.proyectsjala.springbootmongodb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public User registerUser(@RequestBody User user) {
        return userService.addUser(user);
    }


    @GetMapping("/{id}")
    public User getUserById(@PathVariable String id) {
        return userService.getUserById(id);
    }

    @PostMapping("/login")
    public Map<String, String> loginUser(@RequestBody UserCredentials userCredentials) {
        User user = userService.getUserByEmail(userCredentials.getEmail());

        if (user == null) {
            throw new RuntimeException("User not found");
        }

        // Desencriptar y verificar la contraseña
        if (!userService.verifyPassword(userCredentials.getPassword(), user.getPassword())) {
            throw new RuntimeException("Incorrect password");
        }

        // Si la contraseña es correcta, retornar el ID del usuario
        Map<String, String> response = new HashMap<>();
        response.put("userId", user.getIdUser());
        return response;
    }


}
