package com.proyectsjala.springbootmongodb.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.proyectsjala.springbootmongodb.model.Task;
import com.proyectsjala.springbootmongodb.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService service;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Task createTask(@RequestBody Task task){
        return service.addTask(task);
    }

    @GetMapping
    public List<Task> getTasks() {
        return service.findAllTasks();
    }

    @GetMapping("/{id}")
    public Task getTask(@PathVariable String id){
        return service.getTaskById(id);
    }


    @GetMapping("/status")
    public List<Task> getTasksByStatus(@RequestParam List<String> status, @RequestParam String userId){
        return service.getTasksByStatus(status, userId);
    }

    @PutMapping("/{id}")
    public Task modifyTask(@PathVariable String id, @RequestBody Task task){
        return service.updateTask(id, task);
    }

    @DeleteMapping("/{id}")
    public String deleteTask(@PathVariable String id){
        return service.deleteTask(id);
    }

    @PostMapping("/user")
    public List<Task> getTasksByUserId(@RequestBody JsonNode requestBody) {
        String userId = requestBody.get("userId").asText();
        return service.getNonDeletedTasksByUserId(userId);
    }

    @PostMapping("/user/tasks-by-date-range")
    public List<Task> getTasksByUserIdAndDateRange(
            @RequestBody JsonNode requestBody,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate startDate,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate endDate
    ) {
        String userId = requestBody.get("userId").asText();
        return service.getTasksByUserIdAndDateRange(userId, startDate, endDate);
    }

}

