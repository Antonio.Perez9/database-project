package com.proyectsjala.springbootmongodb.service;

import com.proyectsjala.springbootmongodb.model.User;
import com.proyectsjala.springbootmongodb.repository.UserRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;



    public User addUser(User user) {
        if (user.getEmail() == null || !user.getEmail().contains("@")) {
            throw new IllegalArgumentException("Invalid e-mail address ");
        }

        if (userRepository.existsByEmail(user.getEmail())) {
            throw new IllegalArgumentException("The e-mail address is already registered");
        }

        String hashedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedPassword);


        return userRepository.save(user);
    }



    public User getUserById(String id) {
        return userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
    }



    public boolean verifyPassword(String rawPassword, String hashedPassword) {
        return BCrypt.checkpw(rawPassword, hashedPassword);
    }
    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}


