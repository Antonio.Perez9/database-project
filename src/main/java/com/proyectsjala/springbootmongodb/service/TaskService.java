package com.proyectsjala.springbootmongodb.service;

import com.proyectsjala.springbootmongodb.model.Task;
import com.proyectsjala.springbootmongodb.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class TaskService {

    @Autowired
    private TaskRepository repository;




    public Task addTask(Task task) {
        validateTask(task);
        return repository.save(task);
    }

    public List<Task> findAllTasks() {
        return repository.findAll();
    }

    public Task getTaskById(String id){
        return repository.findById(id).orElseThrow(() -> new RuntimeException("Task not found"));
    }


    public List<Task> getTasksByStatus(@RequestParam List<String> status, @RequestParam String userId){
        return repository.findByStatus(status, userId);
    }

    public Task updateTask(String id, Task taskRequest){
        Task existingTask = getTaskById(id);
        validateTask(taskRequest);
        existingTask.setTitle(taskRequest.getTitle());
        existingTask.setDescription(taskRequest.getDescription());
        existingTask.setDateFinally(taskRequest.getDateFinally());
        existingTask.setStatus(taskRequest.getStatus());
        return repository.save(existingTask);
    }



    public List<Task> getTasksByUserId(String idUser) {
        return repository.findByidUser(idUser, Task.Status.DELETED);
    }

    public List<Task> getNonDeletedTasksByUserId(String idUser) {
        List<Task> tasks = repository.findByidUser(idUser, Task.Status.DELETED);
        return tasks.stream()
                .filter(task -> task.getStatus() != Task.Status.DELETED)
                .collect(Collectors.toList());
    }

    public List<Task> getTasksByUserIdAndDateRange(String idUser, LocalDate startDate, LocalDate endDate) {
        return repository.findByidUserAndDateCreationBetween(idUser, startDate.atStartOfDay(), endDate.atTime(23, 59, 59));
    }
    private void validateTask(Task task) {
        if (task == null) {
            throw new IllegalArgumentException("The task cannot be null and void");
        }

        if (task.getTitle() == null || task.getTitle().isEmpty()) {
            throw new IllegalArgumentException("The task title cannot be empty");
        }

    }

    public String deleteTask(String id){
        Task deletedTask = repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Task not found"));

        deletedTask.setStatus(Task.Status.DELETED);
        deletedTask.updateDeletionDate();

        repository.save(deletedTask);
        return id + " task marked as deleted ";
    }
}



