package com.proyectsjala.springbootmongodb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;


@Document(collection = "Task")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    @Id
    private String id;
    private String idUser;
    private String title;
    private String description;
    private LocalDateTime dateCreation;
    private LocalDateTime dateFinally;
    private Status status;
    private LocalDateTime deletionDate;

    // Enum para Status
    public enum Status {
        TODO, DOING, DONE, DELETED
    }

    public void updateDeletionDate() {
        this.deletionDate = LocalDateTime.now();
    }


}
