package com.proyectsjala.springbootmongodb;

import com.proyectsjala.springbootmongodb.model.Task;
import com.proyectsjala.springbootmongodb.repository.TaskRepository;
import com.proyectsjala.springbootmongodb.service.TaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class test15days {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void testDeleteTask() {
        // Crea una tarea de ejemplo
        Task task = new Task();
        task.setTitle("Tarea de ejemplo");
        task.setStatus(Task.Status.TODO);
        task.setDateCreation(LocalDateTime.now());
        taskRepository.save(task);

        // Marca la tarea como "DELETED"
        taskService.deleteTask(task.getId());


        Task deletedTask = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(deletedTask);
        assertEquals(Task.Status.DELETED, deletedTask.getStatus());
        assertNotNull(deletedTask.getDeletionDate());
    }
}
