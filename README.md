# Bases de datos II



## Requisitos Previos

Asegúrate de tener instalado en tu sistema lo siguiente:

    - Java Development Kit (JDK) versión 17 o superior
    - Maeven 7.9
    - Spring boot extension

## Clase a Ejecutar

    - SpringbootmongodbApplication

## Servicio:
    - http://localhost:8080/
## Endpoints

# Tareas

    GET /tasks --> Devuelve todas las tareas
    GET /tasks/status?status={status} --> Devuelve tareas por estado
    POST /tasks --> Crea una nueva tarea
    PUT /tasks/{idTask} --> Actualiza una tarea
    DELETE /tasks/{id} --> Elimina una tarea
    GET /tasks/{id} --> Obtiene una tarea por su ID
    POST /tasks/user/tasks-by-date-range?startDate=2024-04-01&endDate=2024-04-16 --> Filtra tareas por fecha

# Usuarios

    GET /users --> Devuelve todos los usuarios
    POST /users/register --> Registra un nuevo usuario
    GET /users/{id} --> Obtiene un usuario por su ID
    POST /users/login --> Inicia sesión de un usuario, devuelve el ID del usuario